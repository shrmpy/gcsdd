package main

import (
	"bufio"
	"fmt"
	"regexp"
	"strconv"
)

type SDI struct {
	//Drinks map[int]string
	Name             []string
	ID               []string
	StartSize        []int
	FinalSize        []int
	TransactionCount []int
}

// initialize container and empty arrays
func NewSoftDrinkInventory() *SDI {
	return &SDI{
		Name:             initializeString(),
		ID:               initializeString(),
		StartSize:        initializeInt(),
		FinalSize:        initializeInt(),
		TransactionCount: initializeInt(),
	}
}

// populate the arrays according to the data file
func (s *SDI) BuildInventory(scanner *bufio.Scanner) {
	var index int
	var re = regexp.MustCompile(`(\S+)\s+(\d+)\s+(\d+)`)
	for scanner.Scan() {

		m := re.FindStringSubmatch(scanner.Text())
		drink := m[1]
		id := m[2]
		sz, err := strconv.Atoi(m[3])
		if err != nil {
			fmt.Printf("Start inventory is not an integer")
		}

		s.Name[index] = drink
		s.ID[index] = id
		s.StartSize[index] = sz
		s.FinalSize[index] = sz
		index = index + 1
		//TODO index >10, resize slice as needed
	}
	if err := scanner.Err(); err != nil {
		fmt.Printf("failed to scan line %v", err)
	}
}

// calculate inventory by applying transactions
func (s *SDI) ProcessTransactions(scanner *bufio.Scanner) {
	var re = regexp.MustCompile(`\s*(\S+)\s+(\S+)`)
	for scanner.Scan() {

		m := re.FindStringSubmatch(scanner.Text())
		if len(m) < 3 {
			fmt.Printf("invalid transaction: %v\n", m)
			continue
		}
		id := m[1]
		tx, err := strconv.Atoi(m[2])
		if err != nil {
			fmt.Printf("Transaction should be numeric: %v\n", err)
		}

		index, err := s.findID(id)
		if err != nil {
			//fmt.Printf("ID nonexistent\n", err)
			continue
		}

		val := s.TransactionCount[index]
		s.TransactionCount[index] = val + 1
		sz := s.FinalSize[index]
		s.FinalSize[index] = sz + tx
	}
	if err := scanner.Err(); err != nil {
		fmt.Printf("failed to scan line %v", err)
	}

}

func (s *SDI) DisplayReport() {
	// todo - should it sort by ID?
	fmt.Println("Soft drink   ID  Start inventory  Final inventory  # transactions")
	for i, name := range s.Name {
		if name == "" {
			continue
		}
		fmt.Printf("%s  %s  %d  %d  %d\n",
			name, s.ID[i],
			s.StartSize[i],
			s.FinalSize[i],
			s.TransactionCount[i])
	}
}

// with ID, locate index/subscript in order to access the item in arrays
func (s *SDI) findID(id string) (int, error) {
	for k, v := range s.ID {
		if v == id {
			return k, nil
		}
	}
	return -1, fmt.Errorf("ID not found")
}

// probably built-in Golang
func initializeInt() []int {
	return make([]int, 10)
}

// probably built-in Golang
func initializeString() []string {
	return make([]string, 10)
}
