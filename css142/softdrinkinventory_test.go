package main

import (
	"bufio"
	"os"
	"testing"
)

func TestSDI(t *testing.T) {
	inventoryFile := scanner(t, "data6.txt")
	transFile := scanner(t, "data6trans.txt")

	softDrinks := NewSoftDrinkInventory()
	softDrinks.BuildInventory(inventoryFile)
	softDrinks.ProcessTransactions(transFile)
	softDrinks.DisplayReport()
}

func scanner(t *testing.T, path string) *bufio.Scanner {
	f, err := os.Open(path)
	if err != nil {
		t.Errorf("Bad file path (%s): %v", path, err)
		return bufio.NewScanner(os.Stdin)
	}
	// TODO clean up in teardown
	////defer f.Close()

	return bufio.NewScanner(f)
}
